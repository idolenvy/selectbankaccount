//
//  Account.swift
//  SelectBankAccount
//
//  Created by Banyong Jaisue on 23/7/2561 BE.
//  Copyright (c) 2018 SCB. All rights reserved.
//

import UIKit

struct Account {
  let sortSequence: Int?
  let accountName: String?
  let accountNo: String?
  let accountBranch: String?
}
