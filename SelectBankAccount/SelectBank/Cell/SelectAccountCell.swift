//
//  SelectAccountCell.swift
//  SelectBankAccount
//
//  Created by Banyong Jaisue on 24/7/2561 BE.
//  Copyright (c) 2018 SCB. All rights reserved.
//

import UIKit

class SelectAccountCell: UITableViewCell {
  
  @IBOutlet weak var accountNoLabel: UILabel!
  @IBOutlet weak var statusLabel: UILabel!
  @IBOutlet weak var separatorLine: UIView!
  @IBOutlet weak var selectAccountImage: UIImageView!
  
  static var cellId = "SelectAccountCell"
  
  static func registerCell(_ tableView: UITableView) {
    let nib = UINib(nibName: cellId, bundle: Bundle(for: SelectAccountCell.self))
    tableView.register(nib, forCellReuseIdentifier: cellId)
  }
  
  func setCell(with account: SelectBankAccount.GetAccountLists.ViewModel.Account,
               isLast: Bool,
               isSelect: Bool) {
    accountNoLabel.text = account.accountNo
    statusLabel.text = account.accountSelected ? "Current" : "Savings"
    
    separatorLine.isHidden = isLast
    setSelectButton(isSelect: isSelect)
  }
  
  func setSelectButton(isSelect: Bool) {
    let image = isSelect ? UIImage(named: "select_bank_checkbox_check") : UIImage(named: "select_bank_checkbox_uncheck")
    selectAccountImage.image = image
  }
}
