//
//  SelectBankAccountViewController.swift
//  SelectBankAccount
//
//  Created by Banyong Jaisue on 23/7/2561 BE.
//  Copyright (c) 2561 IdolEnvy. All rights reserved.
//

import UIKit

protocol SelectBankAccountViewControllerInterface: class {
  func displayGetAccountLists(viewModel: SelectBankAccount.GetAccountLists.ViewModel)
}

class SelectBankAccountViewController: UIViewController, SelectBankAccountViewControllerInterface {
  var interactor: SelectBankAccountInteractorInterface!
  var router: SelectBankAccountRouter!
  
  @IBOutlet weak var noAccountView: UIView?
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var reviewButton: UIButton!
  
  var accounts: [SelectBankAccount.GetAccountLists.ViewModel.Account] = []
  var selectAccountIndex: Int?

  // MARK: - Object lifecycle

  override func awakeFromNib() {
    super.awakeFromNib()
    configure(viewController: self)
  }

  // MARK: - Configuration

  private func configure(viewController: SelectBankAccountViewController) {
    let router = SelectBankAccountRouter()
    router.viewController = viewController

    let presenter = SelectBankAccountPresenter()
    presenter.viewController = viewController

    let interactor = SelectBankAccountInteractor()
    interactor.presenter = presenter

    viewController.interactor = interactor
    viewController.router = router
  }

  // MARK: - View lifecycle

  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = "Select bank account"
    setUI()
    setUpTableView()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(true)
    
    getAccountLists()
  }
  
  // MARK: - Private
  func setUI() {
    reviewButton.layer.cornerRadius = 4
  }

  // MARK: - Event handling
  func getAccountLists() {
    interactor.getAccountLists(request: SelectBankAccount.GetAccountLists.Request())
  }
  

  // MARK: - Display logic
  func displayGetAccountLists(viewModel: SelectBankAccount.GetAccountLists.ViewModel) {
    if viewModel.accounts.isEmpty {
      noAccountView?.isHidden = false
      tableView?.isHidden = true
    } else {
      noAccountView?.isHidden = true
      tableView?.isHidden = false
    }
    
    accounts = viewModel.accounts
    tableView.reloadData()
  }

  // MARK: - Router

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    router.passDataToNextScene(segue: segue)
  }

  @IBAction func unwindToSelectBankAccountViewController(from segue: UIStoryboardSegue) {
    print("unwind...")
    router.passDataToNextScene(segue: segue)
  }
}
