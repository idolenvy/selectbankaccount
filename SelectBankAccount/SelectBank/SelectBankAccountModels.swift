//
//  SelectBankAccountModels.swift
//  SelectBankAccount
//
//  Created by Banyong Jaisue on 23/7/2561 BE.
//  Copyright (c) 2561 IdolEnvy. All rights reserved.
//

import UIKit

struct SelectBankAccount {
  
  struct GetAccountLists {
    struct Request {}
    struct Response {
      var accounts: [Account]
    }
    struct ViewModel {
      struct Account {
        var accountNo: String
        var accountSelected: Bool
      }
      
      var accounts: [ViewModel.Account]
    }
  }
}
