//
//  SelectBankAccountInteractor.swift
//  SelectBankAccount
//
//  Created by Banyong Jaisue on 23/7/2561 BE.
//  Copyright (c) 2561 IdolEnvy. All rights reserved.
//

import UIKit

protocol SelectBankAccountInteractorInterface {
  func getAccountLists(request: SelectBankAccount.GetAccountLists.Request)
}

class SelectBankAccountInteractor: SelectBankAccountInteractorInterface {
  var presenter: SelectBankAccountPresenterInterface!

  // MARK: - Business logic
  var accounts: [Account] = []

  func getAccountLists(request: SelectBankAccount.GetAccountLists.Request) {
    accounts = getAccounts()
    
    let response = SelectBankAccount.GetAccountLists.Response(accounts: accounts)
    presenter.presentGetAccountLists(response: response)
  }
  
  
  fileprivate func getAccounts() -> [Account] {
//    return []
    let account1 = Account(sortSequence: 0,
                           accountName: "Test1",
                           accountNo: "Test001",
                           accountBranch: "Test00001")
    
    let account2 = Account(sortSequence: 1,
                           accountName: "Test2",
                           accountNo: "Test002",
                           accountBranch: "Test00002")
    
    let account3 = Account(sortSequence: 2,
                           accountName: "Test3",
                           accountNo: "Test003",
                           accountBranch: "Test00003")
    
    return [account1, account2, account3, account3, account3, account3]
  }
}
