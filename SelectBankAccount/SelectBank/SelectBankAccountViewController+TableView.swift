//
//  SelectBankAccountViewController+TableView.swift
//  SelectBankAccount
//
//  Created by Banyong Jaisue on 24/7/2561 BE.
//  Copyright (c) 2018 SCB. All rights reserved.
//

import UIKit

extension SelectBankAccountViewController {
  func setUpTableView() {
    SelectAccountCell.registerCell(tableView)
    tableView.estimatedRowHeight = UITableViewAutomaticDimension
  }
  
  func selectAccount(index: Int) {
    selectAccountIndex = index
    tableView.reloadData()
  }
}

extension SelectBankAccountViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 87.0
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: SelectAccountCell.cellId) as? SelectAccountCell else {
      return UITableViewCell()
    }
    
    let account = accounts[indexPath.row]
    cell.setCell(with: account,
                 isLast: indexPath.row == accounts.count-1,
                 isSelect: indexPath.row == selectAccountIndex)
    
    cell.selectionStyle = .none
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return accounts.count
  }
}

extension SelectBankAccountViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    selectAccount(index: indexPath.row)
  }
}
