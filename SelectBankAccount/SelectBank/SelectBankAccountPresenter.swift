//
//  SelectBankAccountPresenter.swift
//  SelectBankAccount
//
//  Created by Banyong Jaisue on 23/7/2561 BE.
//  Copyright (c) 2561 IdolEnvy. All rights reserved.
//

import UIKit

protocol SelectBankAccountPresenterInterface {
  func presentGetAccountLists(response: SelectBankAccount.GetAccountLists.Response)
}

class SelectBankAccountPresenter: SelectBankAccountPresenterInterface {
  weak var viewController: SelectBankAccountViewControllerInterface!

  // MARK: - Presentation logic

  func presentGetAccountLists(response: SelectBankAccount.GetAccountLists.Response) {
    
    let accounts = response.accounts.map { account -> SelectBankAccount.GetAccountLists.ViewModel.Account in
      guard let accountNo = account.accountName else {
        return SelectBankAccount.GetAccountLists.ViewModel.Account(accountNo: "", accountSelected: false)
      }
      
      return SelectBankAccount.GetAccountLists.ViewModel.Account(accountNo: accountNo, accountSelected: false)
    }
    
    viewController.displayGetAccountLists(viewModel: SelectBankAccount.GetAccountLists.ViewModel(accounts: accounts))
  }
}
